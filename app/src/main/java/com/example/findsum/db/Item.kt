package com.example.findsum.db

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = Item.TABLE_NAME)
data class Item(
    @PrimaryKey val id: Int,
    @ColumnInfo(name = "name") val name: String,
    @ColumnInfo(name = "price") val price: Double
) {
    companion object {
        internal const val TABLE_NAME = "quote_data"
    }
}