package com.example.findsum.db

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import kotlinx.coroutines.flow.Flow

@Dao
abstract class ItemsDao {
    fun getAll(): Flow<MutableList<Item>> = getAllInternal()

    fun delete(item: Item) = deleteInternal(item)

    fun update(items: List<Item>) = updateInternal(items)

    @Query("SELECT * FROM ${Item.TABLE_NAME}")
    protected abstract fun getAllInternal(): Flow<MutableList<Item>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    protected abstract fun updateInternal(items: List<Item>)

    @Delete
    protected abstract fun deleteInternal(item: Item)
}