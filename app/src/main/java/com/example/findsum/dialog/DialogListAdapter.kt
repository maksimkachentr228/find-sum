package com.example.findsum.dialog

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.findsum.R
import com.example.findsum.utils.bind

class DialogListAdapter: RecyclerView.Adapter<DialogListAdapter.Holder>() {

    var list: List<DlgItem> = listOf()
        set (newList) {
            val oldList = field
            DiffUtil.calculateDiff(DiffCallback(oldList, newList)).dispatchUpdatesTo(this)
            field = newList
        }

    override fun getItemCount() = list.size

    override fun getItemViewType(position: Int) = list[position].type

    override fun getItemId(position: Int) = list[position].id.toLong()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        val itemView = when (viewType) {
            DlgItem.TYPE_ITEM -> DlgItemView(parent.context)
            DlgItem.TYPE_HEADER -> ItemViewHeader(parent.context)
            else -> throw Exception("unknown item type: $viewType")
        }

        return Holder(itemView)
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        when(holder.itemViewType) {
            DlgItem.TYPE_ITEM -> {
                val item = list[position] as DlgItemImpl
                (holder.itemView as DlgItemView).apply(item)
            }
            DlgItem.TYPE_HEADER -> {
                val text = (list[position] as DlgItemHeader).headerPrice
                (holder.itemView as ItemViewHeader).applyText(text.toString())
            }
        }
    }

    class DiffCallback(private val oldList: List<DlgItem>, private val newList: List<DlgItem>): DiffUtil.Callback() {
        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int) =
            oldList[oldItemPosition].id == newList[newItemPosition].id

        override fun getOldListSize() = oldList.size
        override fun getNewListSize() = newList.size

        override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int) = false
    }

    class Holder(itemView: ViewGroup): RecyclerView.ViewHolder(itemView)

    abstract class DlgItem(val type: Int, val id: Int) {
        companion object {
            const val TYPE_HEADER = 1
            const val TYPE_ITEM = 2
        }
    }
    class DlgItemImpl(id: Int, val name: String, val price: Double): DlgItem(TYPE_ITEM, id)
    class DlgItemHeader(id: Int, val headerPrice: Double): DlgItem(TYPE_HEADER, id)

    class ItemViewHeader(context: Context): FrameLayout(context) {
        private val textView by bind<TextView>(R.id.textView)
        init {
            LayoutInflater.from(context).inflate(R.layout.list_item_header, this, true)
            isClickable = false
        }

        fun applyText(text: String) { textView.text = text }
    }
}