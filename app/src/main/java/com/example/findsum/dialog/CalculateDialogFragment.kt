package com.example.findsum.dialog

import android.app.AlertDialog
import android.app.Dialog
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.FrameLayout
import android.widget.Toast
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.findsum.R
import com.example.findsum.app
import com.example.findsum.db.Item
import com.example.findsum.utils.FindSubsetsHelper
import com.example.findsum.utils.find
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import kotlin.random.Random

class CalculateDialogFragment: DialogFragment() {

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val container = FrameLayout(requireContext())
        val view = layoutInflater.inflate(R.layout.dialog_calculate, container)

        val sumToCalcEditText = view.find<EditText>(R.id.sumToCalcEditText)!!
        val calcButton = view.find<Button>(R.id.calcButton)!!
        val numListRecyclerView = view.find<RecyclerView>(R.id.numListRecyclerView)!!.apply {
            layoutManager = LinearLayoutManager(requireContext())
            adapter = DialogListAdapter()
        }

        calcButton.setOnClickListener {
            lifecycleScope.launchWhenStarted {
                launch(lifecycleScope.coroutineContext + Dispatchers.Default) {
                    val sum = sumToCalcEditText.text.toString().toDouble()
                    requireContext().app.db.itemsDao().getAll().collect() {
                        val subsets: List<Pair<Double, List<Item>>> = FindSubsetsHelper
                            .findSubsets(sum, it)
                            .map { it.first to it.second.toList() }

                        numListRecyclerView.post {
                            if (subsets.isEmpty()) {
                                Toast.makeText(requireContext(), "Недостаточно позиций для искомой суммы", Toast.LENGTH_LONG)
                                    .show()
                            }
                            (numListRecyclerView.adapter as DialogListAdapter).list = subsets.toDlgList()
                        }
                    }
                }
            }
        }

        return AlertDialog.Builder(activity)
            .setTitle(R.string.dlg_title)
            .setView(view)
            .setNegativeButton(R.string.dlg_close_button, null)
            .create()
    }

    private fun List<Pair<Double, List<Item>>>.toDlgList(): List<DialogListAdapter.DlgItem> {
        val list = mutableListOf<DialogListAdapter.DlgItem>()
        for (pair in this) {
            val id = Random(Int.MAX_VALUE).nextInt()
            list.add(DialogListAdapter.DlgItemHeader(id, pair.first))
            list.addAll(pair.second.map { DialogListAdapter.DlgItemImpl(Random(Int.MAX_VALUE).nextInt(), it.name, it.price) })
        }
        return list
    }
}