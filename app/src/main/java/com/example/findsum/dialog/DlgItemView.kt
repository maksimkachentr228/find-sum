package com.example.findsum.dialog

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.EditText
import android.widget.FrameLayout
import android.widget.ImageButton
import android.widget.TextView
import com.example.findsum.R
import com.example.findsum.utils.bind

class DlgItemView(context: Context, attrs: AttributeSet? = null): FrameLayout(context, attrs) {
    private val nameEditText by bind<EditText>(R.id.nameEditText)
    private val priceEditText by bind<EditText>(R.id.priceEditText)
    private val deleteImageButton by bind<ImageButton>(R.id.deleteImageButton)

    var itemId: Int? = null

    init {
        LayoutInflater.from(context).inflate(R.layout.list_item, this, true)
        layoutParams = LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT)
    }

    fun apply(item: DialogListAdapter.DlgItemImpl) {
        itemId = item.id
        nameEditText.setText(item.name, TextView.BufferType.EDITABLE)
        priceEditText.setText(item.price.toString(), TextView.BufferType.EDITABLE)
        deleteImageButton.visibility = GONE
        nameEditText.keyListener = null
        nameEditText.isCursorVisible = false
        priceEditText.keyListener = null
        priceEditText.isCursorVisible = false
    }
}