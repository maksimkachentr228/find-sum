package com.example.findsum.list

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.animation.ValueAnimator
import android.content.Context
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class DelayedMeasureLayoutManager(context: Context): LinearLayoutManager(context) {
    private var suppressAutoMeasure = false
    lateinit var recyclerContainer: ViewGroup
    private var oldItemsCount = 0

    override fun onAdapterChanged(oldAdapter: RecyclerView.Adapter<*>?, newAdapter: RecyclerView.Adapter<*>?) {
        newAdapter!!.registerAdapterDataObserver(object : RecyclerView.AdapterDataObserver() {
            override fun onItemRangeRemoved(positionStart: Int, itemCount: Int) {
                suppressAutoMeasure = true
                oldItemsCount = newAdapter.itemCount
                super.onItemRangeRemoved(positionStart, itemCount)
            }
        })
        removeAllViews()
    }

    override fun onMeasure(recycler: RecyclerView.Recycler, state: RecyclerView.State, widthSpec: Int, heightSpec: Int) {
        super.onMeasure(recycler, state, widthSpec, heightSpec)

        if (!isAutoMeasureEnabled) {
            requestSimpleAnimationsInNextLayout()
            setMeasuredDimension(width, height)
        }
    }

    override fun onItemsRemoved(recyclerView: RecyclerView, positionStart: Int, itemCount: Int) {
        super.onItemsRemoved(recyclerView, positionStart, itemCount)
        val itemsCount = recyclerView.adapter!!.itemCount

        //to avoid animation when item just replaced in same position
        if (itemsCount != oldItemsCount) {
            val child = recyclerView.getChildAt(itemCount - 1)

            ValueAnimator.ofInt(recyclerContainer.height, recyclerContainer.height - child.measuredHeight).apply {
                startDelay = 200
                addUpdateListener {
                    recyclerContainer.layoutParams.height = it.animatedValue as Int
                    recyclerContainer.requestLayout()
                }

                addListener(object : AnimatorListenerAdapter() {
                    override fun onAnimationEnd(animation: Animator?) {
                        super.onAnimationEnd(animation)
                        recyclerContainer.layoutParams.height = LinearLayout.LayoutParams.WRAP_CONTENT
                        suppressAutoMeasure = false
                        requestLayout()
                    }
                })
            }.start()

        } else {
            postOnAnimation {
                suppressAutoMeasure = false
            }
        }
    }

    override fun supportsPredictiveItemAnimations() = false
    override fun isAutoMeasureEnabled() = !suppressAutoMeasure
}