package com.example.findsum.list

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.FrameLayout
import android.widget.ImageButton
import android.widget.TextView
import com.example.findsum.R
import com.example.findsum.db.Item
import com.example.findsum.utils.bind

class ItemView(context: Context, attrs: AttributeSet? = null): FrameLayout(context, attrs) {

    private val nameEditText by bind<EditText>(R.id.nameEditText)
    private val priceEditText by bind<EditText>(R.id.priceEditText)
    private val deleteImageButton by bind<ImageButton>(R.id.deleteImageButton)

    private val checkButtonNameEditText by bind<ImageButton>(R.id.checkButtonNameEditText)
    private val checkButtonPriceEditText by bind<ImageButton>(R.id.checkButtonPriceEditText)

    lateinit var actionButtonClickListener: OnClickListener
    lateinit var onFieldsChanged: ((name: String, price: Double) -> Unit)
    var itemId: Int? = null

    init {
        LayoutInflater.from(context).inflate(R.layout.list_item, this, true)
        layoutParams = LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT)
    }

    fun apply(item: Item) {
        itemId = item.id
        nameEditText.setText(item.name, TextView.BufferType.EDITABLE)
        priceEditText.setText(item.price.toString(), TextView.BufferType.EDITABLE)
        deleteImageButton.setOnClickListener(actionButtonClickListener)

        nameEditText.setup(checkButtonNameEditText)
        priceEditText.setup(checkButtonPriceEditText)
    }

    private fun EditText.setup(button: ImageButton) {
        var previousString: String? = null
        var isNeedToSave = false
        setOnFocusChangeListener { _, hasFocus ->
            if (hasFocus) {
                previousString = text.toString()

                button.visibility = View.VISIBLE
                button.setOnClickListener {
                    (context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager)
                        .hideSoftInputFromWindow(windowToken,0)
                    isNeedToSave = true
                    // Сразу оба поля. Пофиг, может и не оптимально, пофиг
                    onFieldsChanged(nameEditText.text.toString(), priceEditText.text.toString().toDouble())
                    clearFocus()
                }
            } else {
                button.visibility = View.GONE
                if (!isNeedToSave) reset(previousString!!)
            }
        }
    }

    private fun EditText.reset(previousString: String) {
        setText(previousString)
    }
}