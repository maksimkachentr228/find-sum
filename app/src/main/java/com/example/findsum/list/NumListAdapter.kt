package com.example.findsum.list

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.findsum.db.Item

class NumListAdapter : RecyclerView.Adapter<NumListAdapter.Holder>() {

    companion object {
        const val ADD_BUTTON_TYPE = 0
        const val ITEM_TYPE = 1
    }

    var list: List<Item> = listOf()
        set (newList) {
            val oldList = field
            DiffUtil.calculateDiff(DiffCallback(oldList, newList)).dispatchUpdatesTo(this)
            field = newList
        }

    var onAddButtonClick: (() -> Unit)? = null
    var onActionButtonClickRemove: ((position: Int) -> Unit)? = null
    var onFieldsChange: ((newItem: Item) -> Unit)? = null

    override fun getItemCount() = list.size + 1 // extra position for add button

    private fun isAddButton(position: Int) = position == itemCount - 1

    override fun getItemViewType(position: Int) =
        if (isAddButton(position)) ADD_BUTTON_TYPE else ITEM_TYPE

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        val itemView = when (viewType) {
            ITEM_TYPE -> ItemView(parent.context)
            ADD_BUTTON_TYPE -> ItemViewAddButton(parent.context)
            else -> throw Exception("unknown item type: $viewType")
        }

        if (itemView is ItemView) {
            itemView.actionButtonClickListener = View.OnClickListener {
                onActionButtonClickRemove!!(itemView.itemId!!)
            }
        }

        if (itemView is ItemViewAddButton) {
            itemView.setOnClickListener {
                onAddButtonClick!!()
            }
        }

        return Holder(itemView)
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        val itemView = holder.itemView
        val item = if (!isAddButton(position)) list[position] else null

        if (itemView is ItemView && item != null) {
            // Задаем тут а не в onCreateViewHolder()
            // потому что нам нужен item. Ну и как по-другому сделать я не думал.
            itemView.onFieldsChanged = { name, price ->
                onFieldsChange!!(Item(item.id, name, price))
            }
            itemView.apply(item)
        }
    }

    class DiffCallback(private val oldList: List<Item>, private val newList: List<Item>): DiffUtil.Callback() {
        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int) =
            oldList[oldItemPosition].id == newList[newItemPosition].id

        override fun getOldListSize() = oldList.size
        override fun getNewListSize() = newList.size

        override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int) =
            oldList[oldItemPosition].name == newList[newItemPosition].name
                    && oldList[oldItemPosition].price == newList[newItemPosition].price
    }

    class Holder(itemView: ViewGroup): RecyclerView.ViewHolder(itemView)
}