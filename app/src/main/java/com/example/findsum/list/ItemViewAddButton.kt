package com.example.findsum.list

import android.content.Context
import android.util.AttributeSet
import android.util.TypedValue
import android.view.Gravity
import android.widget.FrameLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.example.findsum.R
import com.example.findsum.utils.dpToPx

class ItemViewAddButton (context: Context, attrs: AttributeSet? = null): FrameLayout(context, attrs) {

    private val drawableSize = context.dpToPx(24)
    private var textView: TextView
    init {
        isClickable = true

        val selectableBackground = TypedValue().let {
            context.theme.resolveAttribute(android.R.attr.selectableItemBackground, it, true)
            it.resourceId
        }

        setBackgroundResource(selectableBackground)

        val margin = context.dpToPx(8)

        textView = TextView(context).apply {
            text = context.getString(R.string.list_add_button)
            textSize = 20f
            layoutParams = LayoutParams(LayoutParams.MATCH_PARENT, context.dpToPx(80)).apply {
                gravity = Gravity.CENTER_VERTICAL
                setMargins(margin, 0, margin, 0)
            }

            val drawable =
                ContextCompat.getDrawable(context, R.drawable.ic_baseline_add_24)!!.apply {
                    setBounds(0, 0, drawableSize, drawableSize)
                    gravity = Gravity.CENTER_VERTICAL
                }
            setCompoundDrawables(drawable, null, null, null)
            compoundDrawablePadding = margin
        }
        addView(textView)
    }
}