package com.example.findsum

import android.app.Application
import android.content.Context
import androidx.room.Room
import com.example.findsum.db.ItemsDatabase

class App : Application() {

    val db by lazy { getDatabase() }

    private fun getDatabase() = Room.databaseBuilder(
            applicationContext,
            ItemsDatabase::class.java, "items-db"
    ).build()
}

inline val Context.app get() = applicationContext as App