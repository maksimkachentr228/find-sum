package com.example.findsum

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.example.findsum.databinding.ActivityMainBinding
import android.widget.LinearLayout
import androidx.lifecycle.lifecycleScope
import com.example.findsum.db.Item
import com.example.findsum.dialog.CalculateDialogFragment
import com.example.findsum.list.DelayedMeasureLayoutManager
import com.example.findsum.list.NumListAdapter
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private val itemsDao by lazy { app.db.itemsDao() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        var list: MutableMap<Int,Item>? = null
        lifecycleScope.launchWhenStarted {
            launch {
                itemsDao.getAll().collect { newList ->
                    list = newList.associateBy { it.id }.toMutableMap()
                    Log.d("MainActivity", "onCreate: newListFromDb=$list")
                    updateListRunnable(newList.toMutableList()).run()
                }
            }
        }

        binding.numListRecyclerView.apply {
            // Опционально.
            // Можно использовать и обычный LinearLayoutManager
            // но тогда анимация удаления будет криво работать
            layoutManager = DelayedMeasureLayoutManager(context).also { lm ->
                lm.recyclerContainer = this@MainActivity.findViewById<LinearLayout>(R.id.recyclerContainer)!!
            }

            adapter = NumListAdapter().apply {
                onAddButtonClick = {
                    if (list == null) {
                        val id = createId(null)
                        list = mutableMapOf(id to Item(id, "", 0.0))
                    } else {
                        val id = createId(list)
                        list!![id] = Item(createId(list), "", 0.0)
                    }
                    lifecycleScope.launchWhenStarted {
                        launch(lifecycleScope.coroutineContext + Dispatchers.Default) {
                            itemsDao.update(list!!.values.toList())
                        }
                    }
                }
                onFieldsChange = { item ->
                    list!![item.id] = item
                    lifecycleScope.launchWhenStarted {
                        launch(lifecycleScope.coroutineContext + Dispatchers.Default) {
                            itemsDao.update(list!!.values.toList())
                        }
                    }
                }
                onActionButtonClickRemove = { id ->
                    lifecycleScope.launchWhenStarted {
                        launch(lifecycleScope.coroutineContext + Dispatchers.Default) {
                            itemsDao.delete(list!![id]!!)
                        }
                    }
                }
            }
        }

        binding.calcFab.setOnClickListener {
            CalculateDialogFragment().show(supportFragmentManager, "DialogCalc")
        }

        setSupportActionBar(binding.toolbar)
    }

    private fun updateListRunnable(newList: MutableList<Item>) = Runnable {
        binding.numListRecyclerView.apply {
            (adapter as NumListAdapter).apply {
                list = newList
            }
        }
    }

    private fun createId(list: Map<Int,Item>?): Int {
        if (list.isNullOrEmpty()) return 0
        return list.keys.last() + 1
    }
}