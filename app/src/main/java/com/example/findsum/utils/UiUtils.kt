package com.example.findsum.utils

import android.content.Context
import android.view.View
import androidx.annotation.IdRes
import kotlin.math.roundToInt

fun <T: View> View.find(@IdRes idRes: Int): T? {
    return findViewById(idRes)
}

fun <T: View> View.bind(@IdRes idRes: Int): Lazy<T> {
    return unsafeLazy { find<T>(idRes)!! }
}

private fun <T> unsafeLazy(initializer: () -> T)
        = lazy(LazyThreadSafetyMode.NONE, initializer)

fun Context.dpToPx(dp: Int) = (dp * resources.displayMetrics.density).roundToInt()