package com.example.findsum.utils;

import android.util.Pair;

import com.example.findsum.db.Item;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class FindSubsetsHelper {
    public static List<Pair<Double,List<Item>>> findSubsets(Double sumToFind, List<Item> items) {
        // Сюда будем записывать найденные подмножества
        ArrayList<Pair<Double,List<Item>>> result = new ArrayList<>();

        // Первый цикл (индекс) определяет первое слогаемое
        for (int i = 0; i < items.size(); i++) {
            // Эта переменная чтобы запоминать с какого числа
            // в прошлый раз шел цикл по поиску подмножества (следующих слогаемых).
            // Зададим ее изначально i+1 чтобы второе слогаемое не было равно первому.
            int prevJ = i+1;

            // До тех пор пока в поиске подмножества не дойдем до предела массива.
            while (prevJ < items.size()) {
                // Сюда будем записывать слогаемые, это и будет подмножество
                ArrayList<Item> list = new ArrayList<>();
                // Изначально добавим первое слогаемое для поиска подмножества
                list.add(items.get(i));
                // Эта переменная определяет текущую сумму (сумму элементов подмножества)
                // из всех найденных слогаемых (не искомую)
                double currentSum = items.get(i).getPrice();

                // Бежим по массиву в поиске слогаемых, если нашли такое, записываем в list
                for (int j = prevJ; j < items.size(); j++) {
                    // Если сумма с этим слогаемое превысит искомую сумму, скипаем
                    if (currentSum + items.get(j).getPrice() > sumToFind) {
                        break;
                    } else {
                        // Иначе добавляем в список и инкрементируем сумму.
                        currentSum += items.get(j).getPrice();
                        list.add(items.get(j));
                    }
                }
                // Запомним откуда шел цикл в прошлый раз (для найденного подмножества)
                // чтобы не начинать с того же места.
                prevJ += 1;
                // Добавим найденное подмножество в результат (если что-то нашли)
                if (list.size() > 1) result.add(new Pair<>(currentSum, list));
            }
        }

        findMostRelevant(sumToFind, result); // уберем лишние (разница > 25% от искомой)
        result.sort(new ResultComparator()); // соритровка по убыванию

        return result;
    }

    public static void findMostRelevant(Double sumToFind, ArrayList<Pair<Double,List<Item>>> result) {
        Double delta = sumToFind - sumToFind/4;
        result.removeIf(pair -> pair.first < delta);
    }

    private static class ResultComparator implements Comparator<Pair<Double, List<Item>>> {
        @Override
        public int compare(Pair<Double, List<Item>> o1, Pair<Double, List<Item>> o2) {
            return o2.first.compareTo(o1.first);
        }
    }
}
