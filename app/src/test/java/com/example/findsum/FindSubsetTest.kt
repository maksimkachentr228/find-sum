package com.example.findsum

import com.example.findsum.db.Item
import com.example.findsum.utils.FindSubsetsHelper
import junit.framework.Assert.assertEquals
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner

@RunWith(RobolectricTestRunner::class)
class FindSubsetTest {
    @Test
    fun `simple numbers small list find sum of 7`() {
        val list = listOf(
            Item(0, "", 1.0),
            Item(1, "", 2.0),
            Item(2, "", 3.0),
            Item(3, "", 5.0),
            Item(4, "", 10.0)
        )
        val expectedResult = listOf(
            android.util.Pair(7.0, listOf(Item(id=1, name="", price=2.0), Item(id=3, name="", price=5.0))),
            android.util.Pair(6.0, listOf(Item(id=0, name="", price=1.0), Item(id=1, name="", price=2.0), Item(id=2, name="", price=3.0))),
            android.util.Pair(6.0, listOf(Item(id=0, name="", price=1.0), Item(id=3, name="", price=5.0))),
        )
        val result = FindSubsetsHelper.findSubsets(7.0, list)
        println(result.map { it.first to it.second }.joinToString(separator = "\n"))
        assertEquals(expectedResult, result)
    }

    @Test
    fun `simple numbers small list find sum of 5`() {
        val list = listOf(
            Item(0, "", 1.0),
            Item(1, "", 2.0),
            Item(2, "", 3.0),
            Item(3, "", 5.0),
            Item(4, "", 10.0)
        )
        val expectedResult = listOf(
            android.util.Pair(5.0, listOf(Item(id=1, name="", price=2.0), Item(id=2, name="", price=3.0))),
            android.util.Pair(4.0, listOf(Item(id=0, name="", price=1.0), Item(id=2, name="", price=3.0))),
        )
        val result = FindSubsetsHelper.findSubsets(5.0, list)
        println(result.map { it.first to it.second }.joinToString(separator = "\n"))
        assertEquals(expectedResult, result)
    }

    @Test
    fun `simple numbers small list find sum of 10`() {
        val list = listOf(
            Item(0, "", 1.0),
            Item(1, "", 2.0),
            Item(2, "", 3.0),
            Item(3, "", 5.0),
            Item(4, "", 10.0)
        )
        val expectedResult = listOf(
            android.util.Pair(10.0, listOf(Item(id=1, name="", price=2.0), Item(id=2, name="", price=3.0), Item(id=3, name="", price=5.0))),
            android.util.Pair(9.0, listOf(Item(id=0, name="", price=1.0), Item(id=2, name="", price=3.0), Item(id=3, name="", price=5.0))),
            android.util.Pair(8.0, listOf(Item(id=2, name="", price=3.0), Item(id=3, name="", price=5.0))),
        )
        val result = FindSubsetsHelper.findSubsets(10.0, list)
        println(result.map { it.first to it.second }.joinToString(separator = "\n"))
        assertEquals(expectedResult, result)
    }

    @Test
    fun `simple numbers small list find sum of 100`() {
        val list = listOf(
            Item(0, "", 10.0),
            Item(1, "", 20.0),
            Item(2, "", 30.0),
            Item(3, "", 40.0)
        )
        val expectedResult = listOf(
            android.util.Pair(100.0, listOf(Item(id=0, name="", price=10.0), Item(id=1, name="", price=20.0), Item(id=2, name="", price=30.0), Item(id=3, name="", price=40.0))),
            android.util.Pair(90.0, listOf(Item(id=1, name="", price=20.0), Item(id=2, name="", price=30.0), Item(id=3, name="", price=40.0))),
            android.util.Pair(80.0, listOf(Item(id=0, name="", price=10.0), Item(id=2, name="", price=30.0), Item(id=3, name="", price=40.0))),
        )
        val result = FindSubsetsHelper.findSubsets(100.0, list)
        println(result.map { it.first to it.second }.joinToString(separator = "\n"))
        assertEquals(expectedResult, result)
    }

    @Test
    fun `hren znaet`() {
        val list = listOf(
            Item(0, "", 68.0), //59
            Item(1, "", 853.0),//68
            Item(2, "", 59.0), //268
            Item(3, "", 268.0) //853
        )
        val result = FindSubsetsHelper.findSubsets(1000.0, list)
        println(result.map { it.first to it.second }.joinToString(separator = "\n"))
    }
}